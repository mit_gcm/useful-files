# Copyright (c) [2019] [Guy Jérémie & Boutay Jean-Marc]
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

import arcpy
import pythonaddins

# Import of the database that allows us to use path-related python functions
# needed to create temporary files for the computation.
import os.path

class ContourButton(object):
    """Implementation for MITgcm_runoff_addin.button (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False
    def onClick(self):
    # We get the name of the toolbox without tbx extension
        toolboxName = "MITgcm_runoff"

    # We get the name of the tool to be executed
        toolName = "Contour"

    # This creates the string with the path to the toolbox
        toolboxPath = os.path.join(os.path.dirname(__file__), toolboxName + ".tbx")


    # The GPToolDialog pythonaddin function calls the geoprocessing Contour tool
        pythonaddins.GPToolDialog(toolboxPath, toolName)


class MergedPolygonButton(object):
    """Implementation for MITgcm_runoff_addin.button_1 (Button)"""
    def __init__(self):
        self.enabled = True
        self.checked = False
    def onClick(self):
    # We get the name of the toolbox without tbx extension
        toolboxName = "MITgcm_runoff"

    # We get the name of the tool to be executed
        toolName = "Polygon"

    # We create the string with the path to the toolbox using os.path database
        toolboxPath = os.path.join(os.path.dirname(__file__), toolboxName + ".tbx")


    # The GPToolDialog pythonaddin function calls the geoprocessing Polygon tool
        pythonaddins.GPToolDialog(toolboxPath, toolName)
