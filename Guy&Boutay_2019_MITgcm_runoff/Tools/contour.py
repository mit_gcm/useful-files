# Copyright (c) [2019] [Guy Jérémie & Boutay Jean-Marc]
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.


# coding: utf-8

# Import first the arcpy functions for python
import arcpy
from arcpy import env
from arcpy.sa import *
import pythonaddins

################################ Fetching the parameters ################################

# To fetch the parameters from the tool, you need to use the arcpy.GetParameterAsText()
# function. The number inside the brackets indicates which parameter you will get.
# You fetch the input parameters and the output parameters the same way.

input = arcpy.GetParameterAsText(0)
sea_level = arcpy.GetParameterAsText(1)
output = arcpy.GetParameterAsText(2)

################################ Calling the function ################################

# To call the function you need to specify the parameters. For this function,
# we set the raster, base contour (contour height) and out contour using the inputs (and output)
# parameters of the tool and we manually set the contour interval.

inRaster = input
contourInterval = 10000
baseContour = sea_level
outContour = output

# We then call the function using the parameters previously defined
Contour(inRaster, outContour, contourInterval, baseContour)
