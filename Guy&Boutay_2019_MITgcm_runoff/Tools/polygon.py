# Copyright (c) [2019] [Guy Jérémie & Boutay Jean-Marc]
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.


# coding: utf-8

# Arcpy functions import
import arcpy
import pythonaddins
from arcpy import env
from arcpy.sa import *

# Import of the database that allows us to use path-related python functions
# needed to create temporary files for the computation.
import os.path
import os

################################ Fetching the parameters ################################

# To fetch the parameters from the tool, you need to use the arcpy.GetParameterAsText()
# funciton. The number inside the brackets indicates which parameter you will get.
# You fetch the input parameters and the output parameters the same way.

contour = arcpy.GetParameterAsText(0)
polygons = arcpy.GetParameterAsText(1)

################################ Creating a temporary file ################################

# In this script, we call 2 functions. As the result of the first function is not needed,
# we do not need to save it somewhere. To do so we create a temporary file.
#
# In this case we create a temporary file inside the script folder. To get the path,
# we use the os.path.join function from the os.path python library. We specify the name of
# the file using the base name of the output parameter (polygons). We did it in order
# to avoid conflict from previous existing files, as the output name is by default
# generated by arcGIS

PolygonName = os.path.basename(os.path.normpath(polygons))
PolygonPath_shp = os.path.join(os.path.dirname(__file__), PolygonName + ".shp")

################################ Calling the functions ################################

# We first call the FeatureToPolygon_management function that computes polygons form a given
# feature layer of the closed contours. We give it as a output parameter the temporary file
# path generated in the previous section.

arcpy.FeatureToPolygon_management(contour,PolygonPath_shp)

# We then call the Dissolve_managment function that merges all the given polygons into
# a single one, using this time the temporary file as an input, and the given output (the one manualy
# specified when you call the tool in the arcGIS software)

arcpy.Dissolve_management(PolygonPath_shp, polygons)

################################ Deleting the temporary files ################################

# When we create a temporary file in the script folder, arcGIS automatically creates a number of files based
# on the .shp file (it creates in total 8 different files - .cpg, .dbf etc.). To avoid the useless accumulation
# of unused files we delete first the .shp file. We then generate paths with the same name as the temporary
# file but with all the different extensions (to have access to the files from the script) and then delete all of them.

os.remove(PolygonPath_shp)
PolygonPath_cpg = os.path.join(os.path.dirname(__file__), PolygonName + ".cpg")
os.remove(PolygonPath_cpg)
PolygonPath_dbf = os.path.join(os.path.dirname(__file__), PolygonName + ".dbf")
os.remove(PolygonPath_dbf)
PolygonPath_prj = os.path.join(os.path.dirname(__file__), PolygonName + ".prj")
os.remove(PolygonPath_prj)
PolygonPath_sbn = os.path.join(os.path.dirname(__file__), PolygonName + ".sbn")
os.remove(PolygonPath_sbn)
PolygonPath_sbx = os.path.join(os.path.dirname(__file__), PolygonName + ".sbx")
os.remove(PolygonPath_sbx)
PolygonPath_shp_xml = os.path.join(os.path.dirname(__file__), PolygonName + ".shp.xml")
os.remove(PolygonPath_shp_xml)
PolygonPath_shx = os.path.join(os.path.dirname(__file__), PolygonName + ".shx")
os.remove(PolygonPath_shx)
